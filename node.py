from flask import Flask, jsonify, request, send_from_directory
from flask_cors import CORS
from argparse import ArgumentParser

from wallet import Wallet
from blockchain import Blockchain

app = Flask(__name__)

CORS(app)


@app.route('/', methods=['GET'])
def get_node_ui():
    return send_from_directory('ui', 'node.html')


@app.route('/network', methods=['GET'])
def get_network_ui():
    return send_from_directory('ui', 'network.html')


@app.route('/hello', methods=['GET'])
def hello():
    return '<h2>Hello world</h2>'


#
# @app.route('/balance', methods=['GET'])
# def get_balance():
#     balance = blockchain.get_balance()
#
#     if balance is None:
#         status = 400
#         response = {
#             'message': 'Loading balance failed',
#             'wallet_set_up': wallet.public_key is not None
#         }
#     else:
#         status = 200
#         response = {
#             'message': 'Fetched balance successfully',
#             'funds': balance
#         }
#
#     return jsonify(response), status


@app.route('/wallet', methods=['POST'])
def create_keys():
    global blockchain

    wallet.create_keys()

    if wallet.save_keys():
        status = 201

        blockchain = Blockchain(wallet.public_key, port)

        response = {
            'public_key': wallet.public_key,
            'private_key': wallet.public_key
        }
    else:
        status = 400
        response = {
            'message': 'Saving the keys failed'
        }

    return jsonify(response), status


@app.route('/wallet', methods=['GET'])
def load_keys():
    global blockchain

    if wallet.load_keys():
        status = 200

        blockchain = Blockchain(wallet.public_key, port)

        response = {
            'public_key': wallet.public_key,
            'private_key': wallet.public_key
        }
    else:
        status = 400
        response = {
            'message': 'Loading the keys failed'
        }

    return jsonify(response), status


@app.route('/broadcast/block', methods=['POST'])
def broadcast_block():
    body = request.get_json()

    if not body:
        response = {
            'message': 'No data found'
        }

        return jsonify(response), 400

    if 'block' not in body:
        response = {
            'message': 'Required data is missing'
        }

        return jsonify(response), 400

    block = body['block']
    block_index = block['index']
    blockchain_last_index = blockchain.chain[-1].index

    if block_index == blockchain_last_index + 1:
        if blockchain.add_block(block):
            response = {
                'message': 'Block added'
            }

            return jsonify(response), 201
        else:
            response = {
                'message': 'Block seems invalid'
            }

            return jsonify(response), 409
    elif block_index > blockchain_last_index + 1:
        response = {
            'message': 'Blockchain seems to differ from local blockchain'
        }

        blockchain.resolve_conflicts = True

        return jsonify(response), 200
    else:
        response = {
            'message': 'Blockchain seems to be shorter, block not added'
        }

        return jsonify(response), 409


@app.route('/mine', methods=['POST'])
def mine():
    if blockchain.resolve_conflicts:
        response = {'message': 'Resolve conflicts first, block not added!'}

        return jsonify(response), 409

    block = blockchain.mine_block()

    if block is not None:
        dict_block = block.__dict__.copy()
        dict_block['transactions'] = [transaction.__dict__.copy() for transaction in dict_block['transactions']]

        response = {
            'message': 'Block added successfully',
            'block': dict_block
        }

        return jsonify(response), 201
    else:
        response = {
            'message': 'Adding a block failed',
            'wallet_set_up': wallet.public_key is not None
        }

        return jsonify(response), 400


@app.route('/resolve/conflicts', methods=['POST'])
def resolve_conflicts():
    replaced = blockchain.resolve()

    if replaced:
        response = {
            'message': 'Chain was replaced!'
        }
    else:
        response = {
            'message': 'Local chain kept'
        }

    return jsonify(response), 200


@app.route('/broadcast/transaction', methods=['POST'])
def broadcast_transaction():
    body = request.get_json()

    if not body:
        response = {
            'message': 'No data found'
        }

        return jsonify(response), 400

    required_fields = ['sender', 'teacher', 'student', 'subject', 'job', 'teacher_token', 'created_at' 'signature',
                       'amount']

    if not all(field in body for field in required_fields):
        response = {
            'message': 'Required data is missing'
        }

        return jsonify(response), 400

    sender = body['sender']
    teacher = body['teacher']
    student = body['student']
    subject = body['subject']
    job = body['job']
    amount = body['amount']
    teacher_token = body['teacher_token']
    created_at = body['created_at']
    signature = body['signature']

    success = blockchain.add_transaction(signature, sender, teacher, student, subject, job, amount, teacher_token,
                                         created_at, True)

    if success:
        response = {
            'message': 'Successfully added transaction',
            'transaction': {
                'sender': sender,
                'teacher': teacher,
                'student': student,
                'subject': subject,
                'job': job,
                'amount': amount,
                'teacher_token': teacher_token,
                'created_at': created_at,
                'signature': signature
            }
        }

        return jsonify(response), 201
    else:
        response = {
            'message': 'Creating a transaction failed'
        }

        return jsonify(response), 400


@app.route('/transaction', methods=['POST'])
def add_transaction():
    if wallet.public_key is None:
        response = {
            'message': 'No wallet set up'
        }

        return jsonify(response), 400

    body = request.get_json()

    if not body:
        response = {
            'message': 'No data found'
        }

        return jsonify(response), 400

    required_fields = ['teacher', 'student', 'subject', 'job', 'amount', 'teacher_token', 'created_at']

    if not all(field in body for field in required_fields):
        response = {
            'message': 'Required data is missing'
        }

        return jsonify(response), 400

    sender = wallet.public_key

    teacher = body['teacher']
    student = body['student']
    subject = body['subject']
    job = body['job']
    amount = body['amount']
    teacher_token = body['teacher_token']
    created_at = body['created_at']

    signature = wallet.sign_transaction(sender, amount)

    success = blockchain.add_transaction(signature, sender, teacher, student, subject, job, amount, teacher_token,
                                         created_at)

    if success:
        response = {
            'message': 'Successfully added transaction',
            'transaction': {
                'sender': sender,
                'teacher': teacher,
                'student': student,
                'subject': subject,
                'job': job,
                'amount': amount,
                'teacher_token': teacher_token,
                'created_at': created_at,
                'signature': signature
            }
        }

        return jsonify(response), 201
    else:
        response = {
            'message': 'Creating a transaction failed'
        }

        return jsonify(response), 400


@app.route('/transactions', methods=['GET'])
def get_open_transaction():
    transactions = [transaction.__dict__.copy() for transaction in blockchain.get_open_transactions()]

    return jsonify(transactions), 200


@app.route('/chain', methods=['GET'])
def get_chain():
    dict_chain = [block.__dict__.copy() for block in blockchain.chain]

    for dict_block in dict_chain:
        dict_block['transactions'] = [transaction.__dict__.copy() for transaction in dict_block['transactions']]

    return jsonify(dict_chain), 200


@app.route('/node', methods=['POST'])
def add_node():
    body = request.get_json()

    if not body:
        response = {
            'message': 'No data found'
        }

        return jsonify(response), 400

    if 'node' not in body:
        response = {
            'message': 'Required data is missing'
        }

        return jsonify(response), 400

    node = body['node']

    blockchain.add_peer_node(node)

    response = {
        'message': 'Node added successfully',
        'all_nodes': blockchain.get_peer_nodes()
    }

    return jsonify(response), 201


@app.route('/node/<node>', methods=['DELETE'])
def remove_node(node):
    if node == '' or node is None:
        response = {
            "message": 'No node found'
        }

        return jsonify(response), 400

    blockchain.remove_peer_node(node)

    response = {
        'message': 'Node removed successfully',
        'all_nodes': blockchain.get_peer_nodes()
    }

    return jsonify(response), 200


@app.route('/nodes', methods=['GET'])
def get_nodes():
    response = {
        'all_nodes': blockchain.get_peer_nodes()
    }

    return jsonify(response), 200


if __name__ == '__main__':
    parser = ArgumentParser()

    parser.add_argument('-p', '--port', type=int, default=4040)

    args = parser.parse_args()

    port = args.port

    wallet = Wallet(port)
    blockchain = Blockchain(wallet.public_key, port)

    app.run(host='127.0.0.1', port=port)
