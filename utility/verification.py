import json
import requests

from sms_link import SMS_LINK
from utility.hash_util import hash_block, hash_string_256


class Verification:
    @classmethod
    def verify_chain(cls, blockchain):
        for (index, block) in enumerate(blockchain):
            if index == 0:
                continue
            if block.previous_hash != hash_block(blockchain[index - 1]):
                return False
            if not cls.valid_proof(block.transactions, block.previous_hash, block.proof):
                return False

        return True

    @staticmethod
    def verify_transaction(transaction):
        url = '{0}/validate/teacher?token={1}'.format(SMS_LINK, transaction.teacher_token)

        try:
            response = requests.get(url)

            body = json.loads(response.content)

            if body is None or body['valid'] is None or body['valid'] is False:
                return False
        except requests.exceptions.ConnectionError:
            return False

        return True

    @classmethod
    def verify_transactions(cls, open_transactions):
        return all([cls.verify_transaction(transaction) for transaction in open_transactions])

    @staticmethod
    def valid_proof(transactions, last_hash, proof):
        guess = (
                str([transaction.to_ordered_dict() for transaction in transactions])
                + str(last_hash)
                + str(proof)
        ).encode()

        guess_hash = hash_string_256(guess)

        return guess_hash[0:2] == '00'
