from collections import OrderedDict

from utility.printable import Printable


class Transaction(Printable):
    def __init__(self, signature, sender, teacher, student, subject, job, amount, teacher_token, created_at):
        self.signature = signature
        self.sender = sender
        self.teacher = teacher
        self.student = student
        self.subject = subject
        self.job = job
        self.amount = amount
        self.teacher_token = teacher_token
        self.created_at = created_at

    def to_ordered_dict(self):
        return OrderedDict([
            ('sender', self.sender),
            ('teacher', self.teacher),
            ('student', self.student),
            ('subject', self.subject),
            ('job', self.job),
            ('amount', self.amount),
            ('teacher_token', self.teacher_token),
            ('created_at', self.created_at)
        ])

    @staticmethod
    def eq(vf, vs):
        return vf.signature == vs.signature \
               and vf.sender == vs.sender \
               and vf.teacher == vs.teacher \
               and vf.student == vs.student \
               and vf.subject == vs.subject \
               and vf.job == vs.job \
               and vf.amount == vs.amount \
               and vf.teacher_token == vs.teacher_token \
               and vf.created_at == vs.created_at
