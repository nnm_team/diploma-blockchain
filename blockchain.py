import json
import requests
from functools import reduce

from block import Block
from wallet import Wallet
from transaction import Transaction
from utility.hash_util import hash_block
from utility.verification import Verification

MINING_REWARD = 10

genesis_block = Block(0, '', [], 100, 0)


class Blockchain:
    def __init__(self, public_key, node_id):
        self.chain = [genesis_block]
        self.__open_transactions = []

        self.public_key = public_key

        self.node_id = node_id
        self.__peer_nodes = set()

        self.resolve_conflicts = False

        self.load_data()

    @property
    def chain(self):
        return self.__chain[:]

    @chain.setter
    def chain(self, val):
        self.__chain = val

    # todo use it
    def update_public_key(self, public_key):
        self.public_key = public_key

    def get_open_transactions(self):
        return self.__open_transactions[:]

    def load_data(self):
        try:
            with open('blockchain-{0}.txt'.format(self.node_id), mode='r') as file:
                file_content = file.readlines()

                if len(file_content) == 0:
                    return self.save_data()

                self.__open_transactions = [
                    Transaction(
                        transaction['signature'],
                        transaction['sender'],
                        transaction['teacher'],
                        transaction['student'],
                        transaction['subject'],
                        transaction['job'],
                        transaction['amount'],
                        transaction['teacher_token'],
                        transaction['created_at']
                    )
                    for transaction in json.loads(file_content[1][:-1])
                ]

                # todo optimize it
                updated_blockchain = []

                for block in json.loads(file_content[0][:-1]):
                    transactions = [
                        Transaction(
                            transaction['signature'],
                            transaction['sender'],
                            transaction['teacher'],
                            transaction['student'],
                            transaction['subject'],
                            transaction['job'],
                            transaction['amount'],
                            transaction['teacher_token'],
                            transaction['created_at']
                        )
                        for transaction in block['transactions']
                    ]

                    updated_block = Block(
                        block['index'],
                        block['previous_hash'],
                        transactions,
                        block['proof'],
                        block['timestamp']
                    )

                    updated_blockchain.append(updated_block)

                self.chain = updated_blockchain

                self.__peer_nodes = set(json.loads(file_content[2]))
        except IOError:
            print('File not find')

            self.save_data()

    def save_data(self):
        try:
            with open('blockchain-{0}.txt'.format(self.node_id), mode='w') as file:
                save_chain = [
                    block.__dict__.copy()
                    for block in
                    [
                        Block(
                            block.index,
                            block.previous_hash,
                            [transaction.__dict__ for transaction in block.transactions],
                            block.proof,
                            block.timestamp)
                        for block in self.__chain
                    ]
                ]
                save_transactions = [transaction.__dict__.copy() for transaction in self.__open_transactions]

                file.write(json.dumps(save_chain))
                file.write('\n')
                file.write(json.dumps(save_transactions))
                file.write('\n')
                file.write(json.dumps(list(self.__peer_nodes)))
        except IOError:
            print('Saving failed!')

    def get_last_element(self):
        if len(self.__chain) == 0:
            return None
        return self.__chain[-1]

    def proof_of_work(self, transactions):
        proof = 0
        last_hash = hash_block(self.get_last_element())

        while not Verification.valid_proof(transactions, last_hash, proof):
            proof += 1

        return proof

    # def get_balance(self, sender=None):
    #     if sender is None:
    #         if self.public_key is None:
    #             return None
    #
    #         participant = self.public_key
    #     else:
    #         participant = sender
    #
    #     sent_transactions = [
    #         [transaction.amount for transaction in block.transactions if transaction.sender == participant]
    #         for block in self.__chain
    #     ]
    #     received_transactions = [
    #         [transaction.amount for transaction in block.transactions if transaction.recipient == participant]
    #         for block in self.__chain
    #     ]
    #
    #     open_sent_transaction = [transaction.amount for transaction in self.__open_transactions if
    #                              transaction.sender == participant]
    #
    #     sent_transactions.append(open_sent_transaction)
    #
    #     amount_sent = reduce(lambda amount, amounts: amount + sum(amounts) if len(amounts) > 0 else amount,
    #                          sent_transactions, 0.0)
    #     amount_received = reduce(lambda amount, amounts: amount + sum(amounts) if len(amounts) > 0 else amount,
    #                              received_transactions, 0.0)
    #
    #     return amount_received - amount_sent

    def add_transaction(self, signature, sender, teacher, student, subject, job, amount, teacher_token, created_at,
                        is_receiving=False):

        transaction = Transaction(signature, sender, teacher, student, subject, job, amount, teacher_token, created_at)

        if Verification.verify_transaction(transaction):
            self.__open_transactions.append(transaction)

            self.save_data()

            if not is_receiving:
                for node in self.__peer_nodes:
                    url = 'http://{0}/broadcast/transaction'.format(node)

                    try:
                        response = requests.post(url, json=transaction.__dict__.copy())

                        if response.status_code >= 400:
                            # todo resolving
                            print('Transaction declined, needs resolving')

                            return False
                    except requests.exceptions.ConnectionError:
                        continue

            return True

        return False

    def add_block(self, block):
        transactions = [
            Transaction(
                transaction['signature'],
                transaction['sender'],
                transaction['teacher'],
                transaction['student'],
                transaction['subject'],
                transaction['job'],
                transaction['amount'],
                transaction['teacher_token'],
                transaction['created_at']
            )
            for transaction in block['transactions']
        ]

        proof_is_valid = Verification.valid_proof(transactions[:-1], block['previous_hash'], block['proof'])

        hashes_match = hash_block(self.get_last_element()) == block['previous_hash']

        if not proof_is_valid or not hashes_match:
            return False

        block_object = Block(block['index'], block['previous_hash'], transactions, block['proof'], block['timestamp'])

        self.__chain.append(block_object)

        stored_transactions = self.__open_transactions[:]

        for block_transaction in transactions:
            for open_transaction in stored_transactions:
                if Transaction.eq(block_transaction, open_transaction):
                    try:
                        self.__open_transactions.remove(open_transaction)
                    except ValueError:
                        print('Item was already removed')

        self.save_data()

        return True

    def mine_block(self):
        if self.public_key is None:
            return None

        if len(self.__open_transactions) <= 0:
            return None

        if not Verification.verify_chain(self.__chain):
            print('chain is invalid')

            return None

        copied_transactions = self.__open_transactions[:]

        hash = hash_block(self.get_last_element())

        proof = self.proof_of_work(copied_transactions)

        if not Wallet.verify_transactions(copied_transactions):
            return None

        block = Block(len(self.__chain), hash, copied_transactions, proof)

        self.__chain.append(block)

        count_of_mined_blocks = len(copied_transactions)

        if count_of_mined_blocks > 0:
            self.__open_transactions = self.__open_transactions[count_of_mined_blocks:]

        self.save_data()

        for node in self.__peer_nodes:
            url = 'http://{0}/broadcast/block'.format(node)

            block_dict = block.__dict__.copy()
            block_dict['transactions'] = [transaction.__dict__.copy() for transaction in block.transactions]

            try:
                response = requests.post(url, json={'block': block_dict})

                if response.status_code == 400 or response.status_code == 500:
                    print('Block declined, needs resolving')

                if response.status_code == 409:
                    self.resolve_conflicts = True
            except requests.exceptions.ConnectionError:
                continue

        return block

    def resolve(self):
        replaced = False
        winner_chain = self.__chain

        for node in self.__peer_nodes:
            url = 'http://{0}/chain'.format(node)

            try:
                response = requests.get(url)

                node_chain = response.json()

                node_chain = [
                    Block(
                        block['index'],
                        block['previous_hash'],
                        [
                            Transaction(
                                transaction['signature'],
                                transaction['sender'],
                                transaction['teacher'],
                                transaction['student'],
                                transaction['subject'],
                                transaction['job'],
                                transaction['amount'],
                                transaction['teacher_token'],
                                transaction['created_at']
                            )
                            for transaction in block['transactions']
                        ],
                        block['proof'],
                        block['timestamp']
                    )
                    for block in node_chain
                ]

                node_chain_len = len(node_chain)
                local_chain_len = len(self.__chain)

                if node_chain_len > local_chain_len and Verification.verify_chain(node_chain):
                    replaced = True
                    winner_chain = node_chain

            except requests.exceptions.ConnectionError:
                continue

        self.__chain = winner_chain
        self.resolve_conflicts = False

        if replaced:
            self.__open_transactions = []

        self.save_data()

        return replaced

    def add_peer_node(self, node):
        self.__peer_nodes.add(node)

        self.save_data()

    def remove_peer_node(self, node):
        self.__peer_nodes.discard(node)

        self.save_data()

    def get_peer_nodes(self):
        return list(self.__peer_nodes)[:]
