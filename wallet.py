from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
import Crypto.Random
import binascii


def generate_keys():
    private_key = RSA.generate(1024, Crypto.Random.new().read)
    public_key = private_key.publickey()

    return (binascii.hexlify(private_key.exportKey(format='DER')).decode('ascii'),
            binascii.hexlify(public_key.exportKey(format='DER')).decode('ascii'))


class Wallet:
    def __init__(self, node_id):
        self.public_key = None
        self.private_key = None

        self.node_id = node_id

    def create_keys(self):
        private_key, public_key = generate_keys()

        self.public_key = public_key
        self.private_key = private_key

    def save_keys(self):
        if self.public_key is None or self.private_key is None:
            return False

        try:
            with open('wallet-{0}.txt'.format(self.node_id), mode='w') as file:
                file.write(self.public_key)
                file.write('\n')
                file.write(self.private_key)
            return True
        except(IOError, IndexError):
            return False

    def load_keys(self):
        try:
            with open('wallet-{0}.txt'.format(self.node_id), mode='r') as file:
                keys = file.readlines()

                self.public_key = keys[0][:-1]
                self.private_key = keys[1]
            return True
        except(IOError, IndexError):
            return False

    def sign_transaction(self, sender, amount):
        signer = PKCS1_v1_5.new(RSA.importKey(binascii.unhexlify(self.private_key)))
        hash = SHA256.new((str(sender) + str(amount)).encode('utf8'))
        signature = signer.sign(hash)

        return binascii.hexlify(signature).decode('ascii')

    @staticmethod
    def verify_transaction(transaction):
        if transaction.sender == 'MINING':
            return True

        public_key = RSA.importKey(binascii.unhexlify(transaction.sender))

        verifier = PKCS1_v1_5.new(public_key)

        hash = SHA256.new((str(transaction.sender) + str(transaction.amount)).encode('utf8'))

        return verifier.verify(hash, binascii.unhexlify(transaction.signature))

    @staticmethod
    def verify_transactions(transactions):
        for transaction in transactions:
            if not Wallet.verify_transaction(transaction):
                return False

        return True
